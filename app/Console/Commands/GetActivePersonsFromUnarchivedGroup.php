<?php

namespace App\Console\Commands;

use App\Models\Group;
use App\Models\Person;
use Illuminate\Console\Command;

class GetActivePersonsFromUnarchivedGroup extends Command
{
    protected $signature = 'unarchived-group:active-users {group}';

    protected $description = 'Command description';

    public function handle()
    {
        $groupId = $this->argument('group');

        $group = Group::with(['people' => function ($q) {
            $q->active();
        }])->unarchived()->find($groupId);

        if (!is_null($group)) {
            $this->table(['id', 'last_name', 'middle_name', 'first_name', 'birth_date', 'active', 'group_id'], $group->people->toArray());
        }
    }
}
