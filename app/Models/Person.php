<?php

namespace App\Models;

use Database\Factories\PersonFactory;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Carbon;

/**
 * Class Person
 * @package App\Models
 * @property-read int $id
 * @property string $last_name
 * @property string $middle_name
 * @property string $first_name
 * @property Carbon $birth_date
 * @property bool $active
 * @property int $group_id
 * @property Group|null $group
 * @mixin Eloquent
 */
class Person extends Model
{
    use HasFactory;

    protected $table = 'people';

    /**
     * @return HasOne
     */
    public function group(): HasOne
    {
        return $this->hasOne(Group::class, 'id', 'group_id');
    }

    /**
     * @return bool
     */
    public function usesTimestamps(): bool
    {
        return false;
    }

    /**
     * @return int
     */
    public function getAgeInYearsAttribute(): int
    {
        $now = Carbon::now();
        return $now->diffInYears($this->birth_date);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeActive(Builder $query): Builder
    {
        return $query->where('active', '=', 1);
    }

    /**
     * @return PersonFactory
     */
    protected static function newFactory(): PersonFactory
    {
        return PersonFactory::new();
    }
}