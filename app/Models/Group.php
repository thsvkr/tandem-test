<?php

namespace App\Models;

use Database\Factories\GroupFactory;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Group
 * @package App\Models
 *
 * @property-read int $id
 * @property string $title
 * @property int $course
 * @property string $faculty
 * @property bool $archive
 * @property Person[]|Collection $people
 *
 * @mixin Eloquent
 */
class Group extends Model
{
    use HasFactory;

    /**
     * @return HasMany
     */
    public function people(): HasMany
    {
        return $this->hasMany(Person::class, 'group_id', 'id');
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeUnarchived(Builder $query): Builder
    {
        return $query->where('archive', '=', 1);
    }

    /**
     * @return GroupFactory
     */
    protected static function newFactory(): GroupFactory
    {
        return GroupFactory::new();
    }

    /**
     * @return bool
     */
    public function usesTimestamps(): bool
    {
        return false;
    }
}