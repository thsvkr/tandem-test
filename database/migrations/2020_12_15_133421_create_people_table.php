<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeopleTable extends Migration
{
    public function up()
    {
        Schema::create(
            'people',
            function (Blueprint $table) {
                $table->bigIncrements('id');

                $table->string('last_name');
                $table->string('middle_name');
                $table->string('first_name');
                $table->timestamp('birth_date');
                $table->boolean('active')->default(0);
                $table->unsignedBigInteger('group_id')->nullable();
                $table->foreign('group_id')->references('id')->on('groups');

                $table->index(['last_name', 'middle_name', 'first_name']);
            }
        );
    }

    public function down()
    {
        Schema::dropIfExists('persons');
    }
}