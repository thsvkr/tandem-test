<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupsTable extends Migration
{
    public function up()
    {
        Schema::create(
            'groups',
            function (Blueprint $table) {
                $table->bigIncrements('id');

                $table->string('title');
                $table->integer('course');
                $table->string('faculty');
                $table->boolean('archive')->default(0);

                $table->index('title');
            }
        );
    }

    public function down()
    {
        Schema::dropIfExists('groups');
    }
}