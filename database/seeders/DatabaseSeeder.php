<?php

namespace Database\Seeders;

use App\Models\Group;
use App\Models\Person;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     * @return void
     */
    public function run()
    {
        Group::factory()
             ->has(Person::factory()->count(4), 'people')
             ->create();
    }
}
