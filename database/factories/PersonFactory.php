<?php

namespace Database\Factories;

use App\Models\Person;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class PersonFactory extends Factory
{
    protected $model = Person::class;

    /**
     * Define the model's default state.
     * @return array
     */
    public function definition(): array
    {
        return [
            'last_name' => $this->faker->lastName,
            'middle_name' => $this->faker->name,
            'first_name' => $this->faker->firstName,
            'birth_date' => Carbon::now()->subYears(rand(18, 50))->subDays(rand(0, 356)),
            'active' => $this->faker->boolean,
            'group_id' => $this->faker->randomNumber(),
        ];
    }
}
