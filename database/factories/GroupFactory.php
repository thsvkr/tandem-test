<?php

namespace Database\Factories;

use App\Models\Group;
use Illuminate\Database\Eloquent\Factories\Factory;

class GroupFactory extends Factory
{
    protected $model = Group::class;

    /**
     * Define the model's default state.
     * @return array
     */
    public function definition(): array
    {
        return [
            'title' => $this->faker->word,
            'course' => $this->faker->randomNumber(),
            'faculty' => $this->faker->word,
            'archive' => $this->faker->boolean,
        ];
    }
}
