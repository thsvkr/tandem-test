<?php

namespace Tests\Models;

use App\Models\Person;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use PHPUnit\Framework\TestCase;

class PersonTest extends TestCase
{
    use RefreshDatabase;

    public function testGetAgeInYearsAttribute()
    {
        $now = Carbon::now();
        $birthDate = $now->subYears(rand(18, 50))->subDays(rand(0, 356));

        $person = Person::factory()->make(['birth_date' => $birthDate]);

        $this->assertEquals($now->diffInYears($birthDate), $person->getAgeInYearsAttribute());
        $this->assertNotEquals($now->addYear()->diffInYears($birthDate), $person->getAgeInYearsAttribute());
    }
}
